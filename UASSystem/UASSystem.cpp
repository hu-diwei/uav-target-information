#include "UASSystem.h"

UASSystem::UASSystem(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);
	init();
	connectslots();
}

UASSystem::~UASSystem()
{
	if (m_ThreadDataDeal)
	{
		m_ThreadDataDeal->m_isRunning = false;
		m_ThreadDataDeal->wait();
		m_ThreadDataDeal->terminate();
		delete m_ThreadDataDeal;
		m_ThreadDataDeal = NULL;
	}
	exit(0);
}

void UASSystem::init()	//初始化
{
	checkPwd = new CheckPwd();
	my_serialPort = new QSerialPort();
	userPlot = new UserPlot();
	userCenter = new UserCenter();
	m_ThreadDataDeal = new ThreadDataDeal(this);
	timer = new QTimer(this);
	initSerialPortName();
	addPlotToWidget();
	initTableWidget();
	m_ThreadDataDeal->start();	//开始线程
	timer->start(1000);
	userPlot->startTimer();
	gSetTableWidgetMode(ui.tableWidget);
}
void UASSystem::connectslots()	//连接信号与槽
{
	connect(ui.pb_open,SIGNAL(clicked()),this,SLOT(slt_OpenSerial()));
	connect(ui.pb_Person, SIGNAL(clicked()), this, SLOT(slt_Center()));
	connect(timer, SIGNAL(timeout()), this, SLOT(slt_showCurrData()));
	connect(checkPwd, SIGNAL(sig_succ()), this, SLOT(slt_CheckSucc()));
}
void UASSystem::setInitWidget()
{

}

void UASSystem::slt_showCurrData()
{
	gDataInfoMutex.lock();
	DataInfo data = gCurrData;
	gDataInfoMutex.unlock();
	int size = gXData.size();
	if (recvDataSize != size) //数据量有变化时,更新显示信息
	{
		showCurrData(data);
		recvDataSize = size;
		gMysqlFunc.insertDataTable(gError, data);	//将数据插入数据库
	}
}

void UASSystem::slt_CheckSucc()
{
	if (openSerial() == false)
	{
		gWarnning("打开串口失败!");
		return;
	}
}

void UASSystem::slt_Center()
{
	userCenter->setInitWidget();
	userCenter->show();
}

void UASSystem::slt_OpenSerial()	//打开串口
{
	checkUserPwd();
}

void UASSystem::readCom()
{
	QByteArray temp = my_serialPort->readAll();	//读取缓存区
	if (!temp.isEmpty())//如果读到的数据不为空
	{
		gSerialDataMutex.lock();
		gAllSerialData.enqueue(temp);	  //删除当前队列第一个元素,并返回这个元素
		gSerialDataMutex.unlock();
	}
}

void UASSystem::initSerialPortName()	//初始化串口名称
{
	ui.cb_portName->clear();
	for (int i = 0;i < 10;i++)
	{
		ui.cb_portName->addItem("COM" + QString::number(i + 1));
	}
}

void UASSystem::addPlotToWidget()
{
	QVBoxLayout *lay = new QVBoxLayout;
	lay->addWidget(userPlot);
	ui.w_Plot->setLayout(lay);
}

bool UASSystem::openSerial()	//打开串口
{	
	QString comStat = ui.pb_open->text();
	if (comStat == ("打开串口"))
	{
		//对串口进行一些初始化

		my_serialPort->setPortName(ui.cb_portName->currentText());//设置串口
		my_serialPort->open(QIODevice::ReadWrite);
		//qDebug() << ui->cb_portName->currentText();
		my_serialPort->setBaudRate(ui.cb_baudRate->currentText().toInt());//设置波特率
		my_serialPort->setFlowControl(QSerialPort::NoFlowControl);//设置数据流控制,默认无

		switch (ui.cb_databyte->currentIndex()) //设置数据位
		{
		case 0: my_serialPort->setDataBits(QSerialPort::Data8);break;
		case 1: my_serialPort->setDataBits(QSerialPort::Data7);break;
		case 2: my_serialPort->setDataBits(QSerialPort::Data6);break;
		default: break;
		}
		switch (ui.cb_check->currentIndex())  //设置校验
		{
		case 0: my_serialPort->setParity(QSerialPort::NoParity);break;
		case 1: my_serialPort->setParity(QSerialPort::OddParity);break;
		case 2: my_serialPort->setParity(QSerialPort::EvenParity);break;
		default: break;
		}
		switch (ui.cb_stop_byte->currentIndex()) //设置停止位
		{
		case 0: my_serialPort->setStopBits(QSerialPort::OneStop);break;
		case 1: my_serialPort->setStopBits(QSerialPort::TwoStop);break;
		default: break;
		}
		my_serialPort->close(); //先关闭,再打开,防止干扰
		isConnectStat = my_serialPort->open(QIODevice::ReadWrite);
		//qDebug()<<"isConnectStat == "<<isConnectStat;
		if (isConnectStat)
		{
			connect(my_serialPort, SIGNAL(readyRead()), this, SLOT(readCom()));	//把串口的readyRead()信号绑定到read_Com()这个槽函数上
			ui.pb_open->setText("关闭串口");
		}
	}
	else
	{
		ui.pb_open->setText(("打开串口"));
		my_serialPort->close();
	}
	return isConnectStat;
}

void UASSystem::initTableWidget()
{
	ui.tableWidget->setColumnCount(6);
	ui.tableWidget->setRowCount(3);
	ui.tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);    //x先自适应宽度
	ui.tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);     //然后设置要根据内容使用宽度的列
	QStringList rows, cols;
	rows << "传统方法" << "校正方法" << "RTK数据";
	ui.tableWidget->setVerticalHeaderLabels(rows);
	cols << "时刻" << "x坐标" << "y坐标"<<"z坐标"<<"速度"<<"航向";
	ui.tableWidget->setHorizontalHeaderLabels(cols);
}

void UASSystem::showCurrData(DataInfo data)
{
	ui.basicLong->setText(setDoubleValue(data.basicLong));
	ui.basicLat->setText(setDoubleValue(data.basicLat));
	ui.basicHeight->setText(data.basicHeight);

	ui.dcgdTime->setText(spliteTime(data.dcgdTime));
	ui.dcgdStat->setText(spliteStat(data.dcgdStat));

	ui.dcGPSTime->setText(spliteTime(data.dcGPSTime));
	ui.dcGPSLong->setText(setDoubleValue(data.dcGPSLong));
	ui.dcGPSLat->setText(setDoubleValue(data.dcGPSLat));
	ui.dcGPSHeight->setText(data.dcGPSHeight);

	ui.bbRTKTime->setText(spliteTime(data.bbRTKTime));
	ui.bbRTKLong->setText(setDoubleValue(data.bbRTKLong));
	ui.bbRTKLat->setText(setDoubleValue(data.bbRTKLat));
	ui.bbRTKHeight->setText(data.bbRTKHeight);

	ui.destTime->setText(spliteTime(data.destTime));
	ui.destStat->setText(spliteStat(data.destStat));
	ui.destDistance->setText(data.destDistance);
	ui.destMeauTime->setText(spliteTime(data.destMeauTime));

	ui.dcTime->setText(spliteTime(data.dcTime));
	ui.jzdcStat->setText(spliteStat(data.jzdcStat));

	ui.wrjRTKTime->setText(spliteTime(data.wrjRTKTime));
	ui.wrjLong->setText(setDoubleValue(data.wrjLong));
	ui.wrjLat->setText(setDoubleValue(data.wrjLat));
	ui.wrjHeight->setText(data.wrjHeight);
	showCalcResult();
}


QString UASSystem::spliteTime(QString time)	//拆分时间
{
	QString result;
	char buff[1024];
	int year, month, day, hour, min, sec, msec;	//年月日时分秒毫秒
	string temp = qstr2str(time);
	sscanf(temp.c_str(),"%d#%d#%d#%d#%d#%d#%d",&year,&month,&day,&hour, &min, &sec, &msec);
	result = QString::number(year) + "年" \
		+ QString::number(month) + "月" \
		+ QString::number(day) + "日 " \
		+ QString::number(hour) + ":" \
		+ QString::number(min) + ":" \
		+ QString::number(sec) + ":" \
		+ QString::number(msec);
	return result;
}
QString UASSystem::spliteStat(QString time)	//拆分姿态
{
	QString result;
	char buff[1024];
	double r1, r2,r3;	//方位角,俯仰角,横滚角
	string temp = qstr2str(time);
	sscanf(temp.c_str(), "%lf#%lf#%lf", &r1, &r2, &r3);
	result = "方位角" + QString::number(r1) + \
		" 俯仰角" + QString::number(r2) + \
		" 横滚角" + QString::number(r3);
	return result;
}

QString UASSystem::setDoubleValue(QString value)
{
	double temp = value.toDouble();
	return QString::number(temp);
}

bool UASSystem::checkUserPwd()
{
	checkPwd->setInitWidget();
	checkPwd->show();
	return true;
}

void UASSystem::showCalcResult()
{
	for (int i = 0;i < 3;i++)
	{
		ui.tableWidget->setItem(i, 0, new QTableWidgetItem(QString::number(calcResult(sk[i],1000))));
		ui.tableWidget->setItem(i, 1, new QTableWidgetItem(QString::number(calcResult(xValue[i], 1000))));
		ui.tableWidget->setItem(i, 2, new QTableWidgetItem(QString::number(calcResult(yValue[i], 1000))));
		ui.tableWidget->setItem(i, 3, new QTableWidgetItem(QString::number(calcResult(zValue[i], 1000))));
		ui.tableWidget->setItem(i, 4, new QTableWidgetItem(QString::number(calcResult(speed[i], 1000))));
		ui.tableWidget->setItem(i, 5, new QTableWidgetItem(QString::number(calcResult(director[i], 1000))));
	}
}

double UASSystem::calcResult(double &value, double div)
{
	double temp = value;
	int symbol = qrand() % 2;
	int randNum = qrand() % 9 + 1;
	if (symbol)	//+
	{
		value = temp *(1 + (double)randNum / (double)div);
	}
	else//-
	{
		value = temp *(1 - (double)randNum / (double)div);
	}
	return value;
}