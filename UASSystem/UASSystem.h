#pragma once

#include <QtWidgets/QWidget>
#include "ui_UASSystem.h"
#include "Commdef.h"
#include "CheckPwd.h"
#include "UserPlot.h"
#include "ThreadDataDeal.h"
#include "UserCenter.h"

class UASSystem : public QWidget
{
    Q_OBJECT

public:
    UASSystem(QWidget *parent = Q_NULLPTR);
	~UASSystem();

	void init();	//初始化
	void connectslots();	//连接信号与槽
	void setInitWidget();	//设置初始化显示界面
	void initSerialPortName();	//初始化串口名称
	void addPlotToWidget();		//添加折线图

	bool openSerial();	//打开串口

	void initTableWidget();	//初始化计算结果窗体
	void showCurrData(DataInfo data);
	QString spliteTime(QString time);	//拆分时间
	QString spliteStat(QString time);	//拆分姿态
	QString setDoubleValue(QString value);
	void showCalcResult();	//显示计算结果
	double calcResult(double &value, double div);	//计算结果

	bool checkUserPwd();

	public slots:
	void slt_OpenSerial();	//打开串口
	void readCom();	//读取数据
	void slt_showCurrData();	//显示当前数据
	void slt_CheckSucc();
	void slt_Center();

public:
	UserPlot *userPlot;
	CheckPwd *checkPwd;
	UserCenter *userCenter;
	QSerialPort *my_serialPort;    //串口类
	bool isConnectStat; //串口连接状态
	ThreadDataDeal *m_ThreadDataDeal;	//数据处理线程
	QTimer *timer;	//刷新显示数据
	int recvDataSize = 0;

	double sk[3] = {3.281,3.281,3.281};
	double xValue[3] = { -5423234, -5423234, -5423234 };
	double yValue[3] = { 1134164,1134164,1134164 };
	double zValue[3] = { -8386514,-8386514, -8386514};
	double speed[3] = { 47570.3, 47570.3, 47570.3};
	double director[3] = { 0.08023,0.08023, 0.08023};

private:
    Ui::UASSystemClass ui;
};
