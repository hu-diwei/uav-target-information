#pragma once

#include <QWidget>
#include "ui_Login.h"
#include "Commdef.h"
#include "UserRegis.h"
#include "UASSystem.h"

class Login : public QWidget
{
	Q_OBJECT

public:
	Login(QWidget *parent = Q_NULLPTR);
	~Login();

	bool initDataBase();	//初始化数据库
	void init();	//初始化
	void connectslots();	//连接信号与槽


	bool searchAllUsers();	//获取所有的用户信息	

	public slots:
	void slt_Regis();	//注册
	void slt_Login();	//登录
	void slt_RegisSucc(); //注册成功

public:
	UserRegis userRegis;	//注册
	UASSystem uas;


private:
	Ui::Login ui;
};
