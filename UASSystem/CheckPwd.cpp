#include "CheckPwd.h"

CheckPwd::CheckPwd(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	setAttribute(Qt::WA_ShowModal);
	this->setWindowFlags(this->windowFlags() | Qt::FramelessWindowHint);
	init();
	connectslots();
}

CheckPwd::~CheckPwd()
{
}

void CheckPwd::init()
{
	ui.le_Pwd->setEchoMode(QLineEdit::Password);
}
void CheckPwd::connectslots()
{
	connect(ui.pb_Check,SIGNAL(clicked()),this,SLOT(slt_Check()));
	connect(ui.pb_Back, SIGNAL(clicked()), this, SLOT(slt_back()));
}

void CheckPwd::setInitWidget()
{
	ui.le_Pwd->setText("");
}

void CheckPwd::slt_Check()
{
	string pwd = qstr2str(ui.le_Pwd->text());
	if (pwd == gCurrLoginUser.userPwd)
	{
		emit sig_succ();
		this->close();
	}
	else
	{
		gWarnning("�������!");
	}
}

void CheckPwd::slt_back()
{
	this->close();
}
