#pragma once

#include <QThread>
#include "Commdef.h"

class ThreadDataDeal : public QThread
{
	Q_OBJECT

public:
	ThreadDataDeal(QObject *parent);
	~ThreadDataDeal();

	void run();

	void dealData(QString data);
	double calcTime(QString time);
	double calcDestLong(QString long1,QString long2);

	bool m_isRunning;	
};
