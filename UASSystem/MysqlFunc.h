#pragma once

#include <QObject>
#include "AllHead.h"

class MysqlFunc : public QObject
{
	Q_OBJECT

public:
	MysqlFunc(QObject *parent);
	MysqlFunc();
	~MysqlFunc();

	bool connectSql(string &error,string ip,string userName,string usrPwd,string databaseName,int port);	//连接mysql数据库
	bool createUserTable(string &error);	//创建用户表
	bool insertUserTable(string &error,User user);	//插入用户
	bool updateUserTable(string &error, User user);	//更新用户信息
	bool searchUsersTable(string &error, QMap<string, User> &users); //查询用户

	bool createDataTable(string &error);	//创建数据表
	bool insertDataTable(string &error, DataInfo data);

	MYSQL mysql;
};
