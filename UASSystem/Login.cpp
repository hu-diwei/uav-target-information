#include "Login.h"

Login::Login(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	init();
	connectslots();
}

Login::~Login()
{
}

void Login::init()	//��ʼ��
{
	ui.le_Pwd->setEchoMode(QLineEdit::Password);
	initDataBase();
}
void Login::connectslots()	//�����ź����
{
	connect(ui.pb_Login,SIGNAL(clicked()),this,SLOT(slt_Login()));
	connect(ui.pb_Regis, SIGNAL(clicked()), this, SLOT(slt_Regis()));
	connect(&userRegis, SIGNAL(sig_succ()), this, SLOT(slt_RegisSucc()));
}

void Login::slt_Regis()
{
	userRegis.setInitWidget();
	userRegis.show();
}

void Login::slt_RegisSucc()
{
	searchAllUsers();
}

void Login::slt_Login()
{
	string userID = qstr2str(ui.le_ID->text());
	string userPwd = qstr2str(ui.le_Pwd->text());
	if (!gAllUsers.contains(userID))
	{
		gWarnning("�˺Ų�����!");
		return;
	}
	gCurrLoginUser = gAllUsers.find(userID).value();
	if (gCurrLoginUser.userPwd != userPwd)
	{
		gWarnning("�������!");
		return;
	}
	this->close();
	uas.show();
}

bool Login::initDataBase()
{
	bool res;
	res = gMysqlFunc.connectSql(gError,"localhost", "root", "1234",\
		"uassystem", 3306);
	if (!res)
	{
		gWarnning(gError);
		return res;
	}
	res = gMysqlFunc.createUserTable(gError);
	if (!res)
	{
		gWarnning(gError);
		return res;
	}
	res = gMysqlFunc.createDataTable(gError);
	if (!res)
	{
		gWarnning(gError);
		return res;
	}
	res = searchAllUsers();
	return res;
}

bool Login::searchAllUsers()
{
	bool res = gMysqlFunc.searchUsersTable(gError, gAllUsers);
	if (!res)
	{
		gWarnning(gError);
		return res;
	}
	return res;
}
