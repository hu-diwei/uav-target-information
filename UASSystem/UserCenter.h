#pragma once

#include <QWidget>
#include "ui_UserCenter.h"
#include "Commdef.h"

class UserCenter : public QWidget
{
	Q_OBJECT

public:
	UserCenter(QWidget *parent = Q_NULLPTR);
	~UserCenter();

	void init();	//初始化
	void connectslots();	//连接信号与槽
	void setInitWidget();	//设置初始化显示界面

	User getUser();	//获取用户
	void clearUserInfo();	//清空用户信息
	void showUserInfo();	//显示当前用户信息


	public slots:
	void slt_Reset();	//修改
	void slt_Back();	//返回

	

private:
	Ui::UserCenter ui;
};
