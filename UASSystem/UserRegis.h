#pragma once

#include <QWidget>
#include "ui_UserRegis.h"
#include "Commdef.h"

class UserRegis : public QWidget
{
	Q_OBJECT

public:
	UserRegis(QWidget *parent = Q_NULLPTR);
	~UserRegis();

	void init();	//初始化
	void connectslots();	//连接信号与槽
	void setInitWidget();	//设置初始化显示界面

	User getUser();	//获取用户
	void clearUserInfo();	//清空用户信息

	public slots:
	void slt_Regis();	//注册
	void slt_Back();	//返回
	

signals:
	void sig_succ();

private:
	Ui::UserRegis ui;
};
