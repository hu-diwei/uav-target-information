#include "MysqlFunc.h"

MysqlFunc::MysqlFunc(QObject *parent)
	: QObject(parent)
{
}

MysqlFunc::MysqlFunc()
{
}

MysqlFunc::~MysqlFunc()
{
}

bool MysqlFunc::connectSql(string &error,string ip, string userName, string usrPwd, string databaseName, int port)
{
	mysql_init(&mysql);
	if (!mysql_real_connect(&mysql, ip.c_str(), userName.c_str(), usrPwd.c_str(), databaseName.c_str(), port, nullptr, 0))
	{
		//cout << "数据库连接失败" << mysql_errno(&mysql) << endl;
		error = "数据库连接失败!" + to_string(mysql_errno(&mysql));
		return false;
	}
	return true;
}

bool MysqlFunc::createUserTable(string &error)
{
	string sql = "create table if not exists Users (id INT NOT NULL AUTO_INCREMENT, UserID varchar(20), \
				UserName varchar(20),UserPwd varchar(20),Gender varchar(20),UserAge int,PRIMARY KEY (id))";
	int re = mysql_query(&mysql, sql.c_str());//从字符串换成const char*
	if (re != 0)
	{
		cout << "mysql_query failed!" << mysql_error(&mysql) << endl;
		error = "创建User表失败!" + to_string(mysql_errno(&mysql));
		return false;
	}
	return true;
}

bool MysqlFunc::insertUserTable(string &error, User user)
{
	char buff[1024];
	sprintf(buff, "insert into Users (UserID,UserName,UserPwd,Gender,UserAge)\
		 values(\'%s\',\'%s\',\'%s\',\'%s\',%d)", (char *)user.userID.c_str(), (char *)user.userName.c_str(), \
		(char *)user.userPwd.c_str(), (char *)user.gender.c_str(), user.age);
	string sql = buff;
	mysql_query(&mysql, "set names gbk");
	int re = mysql_query(&mysql, sql.c_str());//从字符串换成const char*
	if (re != 0)
	{
		cout << "mysql_query failed!" << mysql_error(&mysql) << endl;
		error = "插入User表失败!" + to_string(mysql_errno(&mysql));
		return false;
	}
	return true;
}

bool MysqlFunc::updateUserTable(string &error, User user)
{
	char buff[1024];
	sprintf(buff, "update Users set UserName = \'%s\',UserPwd = \'%s\',Gender = \'%s\',UserAge = %d where UserID = \'%s\'",\
		(char *)user.userName.c_str(), \
		(char *)user.userPwd.c_str(),\
		(char *)user.gender.c_str(), user.age,\
		(char *)user.userID.c_str());
	string sql = buff;
	mysql_query(&mysql, "set names gbk");
	int re = mysql_query(&mysql, sql.c_str());//从字符串换成const char*
	if (re != 0)
	{
		cout << "mysql_query failed!" << mysql_error(&mysql) << endl;
		error = "更新User表失败!" + to_string(mysql_errno(&mysql));
		return false;
	}
	return true;
}

bool MysqlFunc::searchUsersTable(string &error, QMap<string, User> &users)
{
	users.clear();
	User user;
	MYSQL_RES *res = nullptr;///< 创建数据库回应结构体
	MYSQL_ROW row;///< 创建存放结果的结构体
	string sql = "select * from Users";
	mysql_query(&mysql, "set names gbk");
	int re = mysql_query(&mysql, sql.c_str());//从字符串换成const char*
	if (re != 0)
	{
		cout << "mysql_query failed!" << mysql_error(&mysql) << endl;
		error = "查询User表失败!" + to_string(mysql_errno(&mysql));
		return false;
	}
	else
	{
		res = mysql_store_result(&mysql);
		if (nullptr == res)
		{
			error = "装载User数据失败!" + to_string(mysql_errno(&mysql));
			return false;
		}
		else
		{
			///< 取出结果集中内容
			while (row = mysql_fetch_row(res))
			{
				user.id = atoi(row[0]);
				user.userID = row[1];
				user.userName = row[2];
				user.userPwd = row[3];
				user.gender = row[4];
				user.age = atoi(row[5]);
				users[user.userID] = user;
			}
		}
	}
	mysql_free_result(res);//< 释放结果集
	return true;
}


bool MysqlFunc::createDataTable(string &error)	//创建数据表
{
	string sql = "create table if not exists Datas (id INT NOT NULL AUTO_INCREMENT, \
				basicLong varchar(50), basicLat varchar(50),basicHeight varchar(50),\
dcgdTime varchar(50), dcgdStat varchar(50),\
dcGPSTime varchar(50),dcGPSLong varchar(50), dcGPSLat varchar(50),dcGPSHeight varchar(50),\
bbRTKTime varchar(50),bbRTKLong varchar(50), bbRTKLat varchar(50),bbRTKHeight varchar(50),\
destTime varchar(50),destStat varchar(50), destDistance varchar(50),destMeauTime varchar(50),\
dcTime varchar(50), jzdcStat varchar(50),\
wrjRTKTime varchar(50),wrjLong varchar(50), wrjLat varchar(50),wrjHeight varchar(50),\
				PRIMARY KEY (id))";
	int re = mysql_query(&mysql, sql.c_str());//从字符串换成const char*
	if (re != 0)
	{
		cout << "mysql_query failed!" << mysql_error(&mysql) << endl;
		error = "创建Datas表失败!" + to_string(mysql_errno(&mysql));
		return false;
	}
	return true;
}
bool MysqlFunc::insertDataTable(string &error, DataInfo data)
{
	string basicLong = data.basicLong.toStdString();	//基准经度
	string basicLat = data.basicLat.toStdString();	//基准纬度
	string basicHeight = data.basicHeight.toStdString();	//基准高度

	string dcgdTime = data.dcgdTime.toStdString();	//吊舱惯导时间(2022-09-26-13-46-25)
	string dcgdStat = data.dcgdStat.toStdString();	//吊舱惯导姿态(0-0-0)

	string dcGPSTime = data.dcGPSTime.toStdString();	//吊舱GPS时间(2022-09-26-13-46-25)
	string dcGPSLong = data.dcGPSLong.toStdString();	//吊舱GPS经度
	string dcGPSLat = data.dcGPSLat.toStdString();	//吊舱GPS纬度
	string dcGPSHeight = data.dcGPSHeight.toStdString();	//吊舱GPS高度

	string bbRTKTime = data.bbRTKTime.toStdString();	//标靶RTK时间(2022-09-26-13-46-25)
	string bbRTKLong = data.bbRTKLong.toStdString();	//标靶RTK经度
	string bbRTKLat = data.bbRTKLat.toStdString();	//标靶RTK纬度
	string bbRTKHeight = data.bbRTKHeight.toStdString();	//标靶RTK高度

	string destTime = data.destTime.toStdString();	//目标时间(2022-09-26-13-46-25)
	string destStat = data.destStat.toStdString();		//目标姿态(1-1-1)
	string destDistance = data.destDistance.toStdString();	//目标距离
	string destMeauTime = data.destMeauTime.toStdString();	//目标距离测量时间(2022-09-26-13-46-25)

	string dcTime = data.dcTime.toStdString();		//吊舱时间(2022-09-26-13-46-25)
	string jzdcStat = data.jzdcStat.toStdString();		//机载吊舱姿态(1-1-1)

	string wrjRTKTime = data.wrjRTKTime.toStdString();		//无人机RTK时间(2022-09-26-13-46-25)
	string wrjLong = data.wrjLong.toStdString();	//无人机经度
	string wrjLat = data.wrjLat.toStdString();	//无人机纬度
	string wrjHeight = data.wrjHeight.toStdString();	//无人机高度
	char buff[4096];
	sprintf(buff, "insert into Datas (basicLong,basicLat,basicHeight,\
	dcgdTime,dcgdStat,dcGPSTime,dcGPSLong,dcGPSLat,dcGPSHeight,\
bbRTKTime,bbRTKLong,bbRTKLat,bbRTKHeight,\
destTime,destStat,destDistance,destMeauTime,\
dcTime,jzdcStat,wrjRTKTime,wrjLong,wrjLat,wrjHeight)\
		 values(\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\
		\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\')", \
		(char *)basicLong.c_str(), (char *)basicLat.c_str(), (char *)basicHeight.c_str(), \
		(char *)dcgdTime.c_str(), (char *)dcgdStat.c_str(), \
		(char *)dcGPSTime.c_str(), (char *)dcGPSLong.c_str(), (char *)dcGPSLat.c_str(), (char *)dcGPSHeight.c_str(), \
		(char *)bbRTKTime.c_str(), (char *)bbRTKLong.c_str(), (char *)bbRTKLat.c_str(), (char *)bbRTKHeight.c_str(), \
		(char *)destTime.c_str(), (char *)destStat.c_str(), (char *)destDistance.c_str(), (char *)destMeauTime.c_str(), \
		(char *)dcTime.c_str(), (char *)jzdcStat.c_str(), \
		(char *)wrjRTKTime.c_str(), (char *)wrjLong.c_str(), (char *)wrjLat.c_str(), (char *)wrjHeight.c_str());
	string sql = buff;
	mysql_query(&mysql, "set names gbk");
	int re = mysql_query(&mysql, sql.c_str());//从字符串换成const char*
	if (re != 0)
	{
		cout << "mysql_query failed!" << mysql_error(&mysql) << endl;
		error = "插入Datas表失败!" + to_string(mysql_errno(&mysql));
		return false;
	}
	return true;
}