#include "UserRegis.h"

UserRegis::UserRegis(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	init();
	connectslots();
}

UserRegis::~UserRegis()
{
}


void UserRegis::init()	//初始化
{
	ui.rb_Man->setChecked(true);
	ui.le_Pwd->setEchoMode(QLineEdit::Password);
}
void UserRegis::connectslots()	//连接信号与槽
{
	connect(ui.pb_Back, SIGNAL(clicked()), this, SLOT(slt_Back()));
	connect(ui.pb_Regis, SIGNAL(clicked()), this, SLOT(slt_Regis()));
}
void UserRegis::setInitWidget()
{
	gMysqlFunc.searchUsersTable(gError, gAllUsers);
}

void UserRegis::slt_Regis()	//注册
{
	User user = getUser();
	if (user.userID == "" || user.userPwd == "")
	{
		gWarnning("存在未填写项,注册失败!");
		return;
	}
	gMysqlFunc.insertUserTable(gError, user);
	emit sig_succ();
	return;
	
}
void UserRegis::slt_Back()	//返回
{
	emit sig_succ();
	this->close();
}

User UserRegis::getUser()	//获取用户
{
	User user;
	user.userID = qstr2str(ui.le_ID->text());
	user.userName = qstr2str(ui.le_Name->text());
	user.userPwd = qstr2str(ui.le_Pwd->text());
	user.gender = qstr2str("男");
	if (ui.rb_Women->isChecked())
	{
		user.gender = qstr2str("女");
	}
	user.age = ui.le_Age->text().toInt();
	return user;
}
void UserRegis::clearUserInfo()	//清空用户信息
{
	ui.le_ID->setText("");
	ui.le_Pwd->setText("");
	ui.le_Name->setText("");
	ui.le_Age->setText("");
}