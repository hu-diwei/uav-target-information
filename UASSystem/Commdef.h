#pragma once

#include "AllHead.h"
#include "MysqlFunc.h"

extern string gError;
extern MysqlFunc gMysqlFunc;
extern User gCurrLoginUser;
extern QMap<string, User> gAllUsers;
extern QQueue<QString> gAllSerialData;	//接收串口数据
extern DataInfo gCurrData;	//当前数据信息
extern QMutex gSerialDataMutex;	//串口数据队列锁
extern QMutex gDataInfoMutex;	//当前数据锁
extern QVector<double> gXData, gYData;	//曲线数据
extern QMutex plotMutex;	//曲线锁

extern void gWarnning(string info);	//警告信息
extern void gSetTableWidgetMode(QTableWidget *tw);
extern QString str2qstr(const string str);
extern string qstr2str(const QString qstr);