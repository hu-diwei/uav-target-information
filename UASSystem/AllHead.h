#pragma once
#pragma execution_character_set("utf-8")

//serialport
#include <QtSerialPort/QSerialPort>

#include <iostream>
#include <QDebug>
#include <time.h>
#include <string>
#include <fstream>
#include <sstream>
#include <io.h>
#include <direct.h> 

#include <QDir>
#include <QFile>
#include <QSettings>
#include <QProcess>
#include <QTimer>
#include <QTextCodec>
#include <QPixmap>
#include <QPainter>

#include <windows.h>

#include <QDateTime>
#include <QMessageBox>
#include <QUuid>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QQueue>
#include <QMap>
#include <QVector>
#include <QTableWidget>
#include <QMutex>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <queue>

using namespace std;


#pragma comment(lib,"libmysql.lib")
#include <mysql.h>

class User
{
public:
	int id;
	string userID;	//工号
	string userName;	//姓名
	string userPwd;		//密码
	string gender;	//性别
	int age;	//年龄
};


class DataInfo
{
public:
	QString basicLong;	//基准经度
	QString basicLat;	//基准纬度
	QString basicHeight;	//基准高度

	QString dcgdTime;	//吊舱惯导时间(2022-09-26-13-46-25)
	QString dcgdStat;	//吊舱惯导姿态(0-0-0)

	QString dcGPSTime;	//吊舱GPS时间(2022-09-26-13-46-25)
	QString dcGPSLong;	//吊舱GPS经度
	QString dcGPSLat;	//吊舱GPS纬度
	QString dcGPSHeight;	//吊舱GPS高度

	QString bbRTKTime;	//标靶RTK时间(2022-09-26-13-46-25)
	QString bbRTKLong;	//标靶RTK经度
	QString bbRTKLat;	//标靶RTK纬度
	QString bbRTKHeight;	//标靶RTK高度

	QString destTime;	//目标时间(2022-09-26-13-46-25)
	QString destStat;		//目标姿态(1-1-1)
	QString destDistance;	//目标距离
	QString destMeauTime;	//目标距离测量时间(2022-09-26-13-46-25)

	QString dcTime;		//吊舱时间(2022-09-26-13-46-25)
	QString jzdcStat;		//机载吊舱姿态(1-1-1)

	QString wrjRTKTime;		//无人机RTK时间(2022-09-26-13-46-25)
	QString wrjLong;	//无人机经度
	QString wrjLat;	//无人机纬度
	QString wrjHeight;	//无人机高度

	DataInfo()
	{
		basicLong = "0";
		basicLat = "0";
		basicHeight = "0";

		dcgdTime = "0#0#0#0#0#0#0";
		dcgdStat = "0#0#0";	//吊舱惯导姿态(0#0#0)

		dcGPSTime = "0#0#0#0#0#0#0";	//吊舱GPS时间(2022#09#26#13#46#25)
		dcGPSLong = "0";	//吊舱GPS经度
		dcGPSLat = "0";	//吊舱GPS纬度
		dcGPSHeight = "0";	//吊舱GPS高度

		bbRTKTime = "0#0#0#0#0#3#785";	//标靶RTK时间(2022#09#26#13#46#25)
		bbRTKLong = "0.00000175791";	//标靶RTK经度
		bbRTKLat = "0.00000180956";	//标靶RTK纬度
		bbRTKHeight = "-509486";	//标靶RTK高度

		destTime = "0#0#0#0#0#3#882";	//目标时间(2022#09#26#13#46#25)
		destStat = "2.67954#1.56527#0";		//目标姿态(1#1#1)
		destDistance = "3.2695";	//目标距离
		destMeauTime = "0#0#0#0#0#3#847";	//目标距离测量时间(2022#09#26#13#46#25)

		dcTime = "0#0#0#0#0#3#785";		//吊舱时间(2022#09#26#13#46#25)
		jzdcStat = "3.43154#0#0";		//机载吊舱姿态(1#1#1)

		wrjRTKTime = "0#0#0#0#0#3#785";		//无人机RTK时间(2022#09#26#13#46#25)
		wrjLong = "0.00000175791";	//无人机经度
		wrjLat = "0.00000180956";	//无人机纬度
		wrjHeight = "-509488";	//无人机高度
	}
};