#pragma once

#include <QWidget>
#include "ui_CheckPwd.h"
#include "Commdef.h"

class CheckPwd : public QWidget
{
	Q_OBJECT

public:
	CheckPwd(QWidget *parent = Q_NULLPTR);
	~CheckPwd();

	void init();
	void connectslots();
	void setInitWidget();

	public slots:
	void slt_Check();
	void slt_back();

signals:
	void sig_succ();

private:
	Ui::CheckPwd ui;
};
