#include "UserPlot.h"

UserPlot::UserPlot(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	init();
	connectslots();
}

UserPlot::~UserPlot()
{
	if (timer)
	{
		delete timer;
	}
	if (m_Plot)
	{
		delete m_Plot;
	}
}

void UserPlot::init()	//初始化
{
	m_Plot = new QCustomPlot();
	timer = new QTimer();
	initPlot();
	addPlotToWidget();
}
void UserPlot::connectslots()	//连接信号与槽
{
	connect(timer,SIGNAL(timeout()),this,SLOT(slt_flushData()));
}
void UserPlot::setInitWidget()	//设置初始化显示界面
{

}

void UserPlot::slt_flushData()
{
	flushPlot();
}

void UserPlot::flushPlot()
{
	QVector<double> xData, yData;
	QVector<double> xData1, yData1;
	plotMutex.lock();
	xData = gXData;
	yData = gYData;
	plotMutex.unlock();
	if (xData.size() == 0)
	{
		return;
	}
	double MaxX = getMaxXY(xData);
	double MaxY = getMaxXY(yData);
	double maxCenter = (xData[xData.size() - 1])*4/5;
	for (int i = 0;i < xData.size();i++)
	{
		xData1.push_back(maxCenter);
		yData1.push_back(yData[i]);
	}
	yData1[0] = 0;
	yData1[yData1.size() - 1] = MaxY;

	m_Plot->xAxis->setRange(0, MaxX*1.01); //设置坐标轴范围
	m_Plot->yAxis->setRange(0, MaxY*1.01); //设置坐标轴范围

	m_Plot->graph(0)->setPen(QPen(QColor(0, 0, 255)));
	m_Plot->graph(0)->setData(xData, yData);

	m_Plot->graph(1)->setPen(QPen(QColor(255, 0, 0)));
	m_Plot->graph(1)->setData(xData1, yData1);

	m_Plot->replot();
}

void UserPlot::addPlotToWidget()		//添加折线图
{
	QVBoxLayout *lay = new QVBoxLayout;
	lay->addWidget(m_Plot);
	ui.w_Plot->setLayout(lay);
}

void UserPlot::startTimer()	//启动定时器
{
	timer->start(1000);
}
void UserPlot::stopTimer()	//停止定时器
{
	timer->stop();
}

void UserPlot::initPlot()
{
	QCPTextElement *m_title;

	m_Plot->xAxis->setTicks(true); //禁用自动刻度-如果不禁用就无法设置自定义刻度
	m_Plot->xAxis->setTickLabels(true);//禁用自动刻度标签
	m_Plot->xAxis->setLabel("时间");//
	m_Plot->yAxis->setNumberFormat("f");
	m_Plot->yAxis->setNumberPrecision(1);
	m_Plot->xAxis->setTickLength(2, 2);//设置刻度长度，第一个是向内的长度，第二个是向外的长度
	m_Plot->xAxis->setRange(0, 11); //设置坐标轴范围

	//y轴同理，这里简单设置一下
	m_Plot->yAxis->setLabel("度");
	m_Plot->yAxis->setNumberFormat("e");
	m_Plot->yAxis->setNumberPrecision(1);
	m_Plot->yAxis->setTickLength(1, 0);
	m_Plot->yAxis->setRange(0, 12);
	//m_Plot->yAxis->setPadding(50);

	m_Plot->addGraph();
	m_Plot->addGraph();
	
	m_Plot->graph(1)->setName("目标经度-目标时间");
}

double UserPlot::getMaxXY(QVector<double> datas)
{
	double maxValue = datas[0];
	for (int i = 1;i < datas.size();i++)
	{
		if (maxValue < datas[i])
		{
			maxValue = datas[i];
		}
	}
	return maxValue;
}