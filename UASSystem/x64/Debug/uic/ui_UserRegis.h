/********************************************************************************
** Form generated from reading UI file 'UserRegis.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERREGIS_H
#define UI_USERREGIS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserRegis
{
public:
    QGridLayout *gridLayout_6;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QWidget *widget;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_3;
    QPushButton *pb_Regis;
    QSpacerItem *horizontalSpacer;
    QPushButton *pb_Back;
    QGridLayout *gridLayout_2;
    QLabel *label_2;
    QLineEdit *le_Pwd;
    QRadioButton *rb_Man;
    QRadioButton *rb_Women;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *le_ID;
    QGridLayout *gridLayout_4;
    QLabel *label_3;
    QLineEdit *le_Age;
    QGridLayout *gridLayout_7;
    QLabel *label_4;
    QLineEdit *le_Name;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *UserRegis)
    {
        if (UserRegis->objectName().isEmpty())
            UserRegis->setObjectName(QStringLiteral("UserRegis"));
        UserRegis->resize(495, 474);
        gridLayout_6 = new QGridLayout(UserRegis);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        verticalSpacer = new QSpacerItem(20, 127, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(98, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_2, 1, 0, 1, 1);

        widget = new QWidget(UserRegis);
        widget->setObjectName(QStringLiteral("widget"));
        gridLayout_5 = new QGridLayout(widget);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        pb_Regis = new QPushButton(widget);
        pb_Regis->setObjectName(QStringLiteral("pb_Regis"));

        gridLayout_3->addWidget(pb_Regis, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 0, 0, 1, 1);

        pb_Back = new QPushButton(widget);
        pb_Back->setObjectName(QStringLiteral("pb_Back"));

        gridLayout_3->addWidget(pb_Back, 0, 2, 1, 1);


        gridLayout_5->addLayout(gridLayout_3, 5, 0, 1, 2);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);

        le_Pwd = new QLineEdit(widget);
        le_Pwd->setObjectName(QStringLiteral("le_Pwd"));

        gridLayout_2->addWidget(le_Pwd, 0, 1, 1, 1);


        gridLayout_5->addLayout(gridLayout_2, 1, 0, 1, 2);

        rb_Man = new QRadioButton(widget);
        rb_Man->setObjectName(QStringLiteral("rb_Man"));

        gridLayout_5->addWidget(rb_Man, 3, 0, 1, 1);

        rb_Women = new QRadioButton(widget);
        rb_Women->setObjectName(QStringLiteral("rb_Women"));

        gridLayout_5->addWidget(rb_Women, 3, 1, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        le_ID = new QLineEdit(widget);
        le_ID->setObjectName(QStringLiteral("le_ID"));

        gridLayout->addWidget(le_ID, 0, 1, 1, 1);


        gridLayout_5->addLayout(gridLayout, 0, 0, 1, 2);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_4->addWidget(label_3, 0, 0, 1, 1);

        le_Age = new QLineEdit(widget);
        le_Age->setObjectName(QStringLiteral("le_Age"));

        gridLayout_4->addWidget(le_Age, 0, 1, 1, 1);


        gridLayout_5->addLayout(gridLayout_4, 4, 0, 1, 2);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        label_4 = new QLabel(widget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_7->addWidget(label_4, 0, 0, 1, 1);

        le_Name = new QLineEdit(widget);
        le_Name->setObjectName(QStringLiteral("le_Name"));

        gridLayout_7->addWidget(le_Name, 0, 1, 1, 1);


        gridLayout_5->addLayout(gridLayout_7, 2, 0, 1, 2);


        gridLayout_6->addWidget(widget, 1, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(98, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_3, 1, 2, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 128, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer_2, 2, 1, 1, 1);


        retranslateUi(UserRegis);

        QMetaObject::connectSlotsByName(UserRegis);
    } // setupUi

    void retranslateUi(QWidget *UserRegis)
    {
        UserRegis->setWindowTitle(QApplication::translate("UserRegis", "UserRegis", Q_NULLPTR));
        pb_Regis->setText(QApplication::translate("UserRegis", "\346\263\250\345\206\214", Q_NULLPTR));
        pb_Back->setText(QApplication::translate("UserRegis", "\350\277\224\345\233\236", Q_NULLPTR));
        label_2->setText(QApplication::translate("UserRegis", "\350\257\267\350\276\223\345\205\245\345\257\206\347\240\201:", Q_NULLPTR));
        rb_Man->setText(QApplication::translate("UserRegis", "\347\224\267", Q_NULLPTR));
        rb_Women->setText(QApplication::translate("UserRegis", "\345\245\263", Q_NULLPTR));
        label->setText(QApplication::translate("UserRegis", "\350\257\267\350\276\223\345\205\245\350\264\246\345\217\267:", Q_NULLPTR));
        label_3->setText(QApplication::translate("UserRegis", "\350\257\267\350\276\223\345\205\245\345\271\264\351\276\204:", Q_NULLPTR));
        label_4->setText(QApplication::translate("UserRegis", "\350\257\267\350\276\223\345\205\245\345\247\223\345\220\215:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UserRegis: public Ui_UserRegis {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERREGIS_H
