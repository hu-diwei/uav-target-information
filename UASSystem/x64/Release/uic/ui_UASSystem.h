/********************************************************************************
** Form generated from reading UI file 'UASSystem.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UASSYSTEM_H
#define UI_UASSYSTEM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UASSystemClass
{
public:
    QGridLayout *gridLayout_30;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *comboBox_portName;
    QComboBox *cb_portName;
    QHBoxLayout *horizontalLayout_14;
    QLabel *comboBox_baudRate;
    QComboBox *cb_baudRate;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_3;
    QComboBox *cb_stop_byte;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_2;
    QComboBox *cb_databyte;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label;
    QComboBox *cb_check;
    QPushButton *pb_open;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_24;
    QTableWidget *tableWidget;
    QWidget *widget;
    QGridLayout *gridLayout_29;
    QWidget *widget_2;
    QGridLayout *gridLayout_25;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout_27;
    QLabel *label_25;
    QLineEdit *basicLat;
    QGridLayout *gridLayout_28;
    QLabel *label_26;
    QLineEdit *basicHeight;
    QGridLayout *gridLayout_26;
    QLabel *label_24;
    QLineEdit *basicLong;
    QPushButton *pb_Person;
    QGroupBox *groupBox1;
    QGridLayout *gridLayout_23;
    QGridLayout *gridLayout_3;
    QLabel *label_4;
    QLineEdit *dcgdTime;
    QGridLayout *gridLayout_4;
    QLabel *label_5;
    QLineEdit *dcgdStat;
    QGridLayout *gridLayout_5;
    QLabel *label_6;
    QLineEdit *dcGPSTime;
    QGridLayout *gridLayout_6;
    QLabel *label_7;
    QLineEdit *dcGPSLong;
    QGridLayout *gridLayout_7;
    QLabel *label_8;
    QLineEdit *dcGPSLat;
    QGridLayout *gridLayout_8;
    QLabel *label_9;
    QLineEdit *dcGPSHeight;
    QGridLayout *gridLayout_9;
    QLabel *label_10;
    QLineEdit *bbRTKTime;
    QGridLayout *gridLayout_10;
    QLabel *label_11;
    QLineEdit *bbRTKLong;
    QGridLayout *gridLayout_11;
    QLabel *label_12;
    QLineEdit *bbRTKLat;
    QGridLayout *gridLayout_12;
    QLabel *label_13;
    QLineEdit *bbRTKHeight;
    QGridLayout *gridLayout_13;
    QLabel *label_14;
    QLineEdit *destTime;
    QGridLayout *gridLayout_14;
    QLabel *label_15;
    QLineEdit *destStat;
    QGridLayout *gridLayout_15;
    QLabel *label_16;
    QLineEdit *destDistance;
    QGridLayout *gridLayout_16;
    QLabel *label_17;
    QLineEdit *destMeauTime;
    QGridLayout *gridLayout_17;
    QLabel *label_18;
    QLineEdit *dcTime;
    QGridLayout *gridLayout_18;
    QLabel *label_19;
    QLineEdit *jzdcStat;
    QGridLayout *gridLayout_19;
    QLabel *label_20;
    QLineEdit *wrjRTKTime;
    QGridLayout *gridLayout_20;
    QLabel *label_21;
    QLineEdit *wrjLong;
    QGridLayout *gridLayout_21;
    QLabel *label_22;
    QLineEdit *wrjLat;
    QGridLayout *gridLayout_22;
    QLabel *label_23;
    QLineEdit *wrjHeight;
    QWidget *w_Plot;

    void setupUi(QWidget *UASSystemClass)
    {
        if (UASSystemClass->objectName().isEmpty())
            UASSystemClass->setObjectName(QStringLiteral("UASSystemClass"));
        UASSystemClass->resize(1651, 900);
        gridLayout_30 = new QGridLayout(UASSystemClass);
        gridLayout_30->setSpacing(6);
        gridLayout_30->setContentsMargins(11, 11, 11, 11);
        gridLayout_30->setObjectName(QStringLiteral("gridLayout_30"));
        groupBox = new QGroupBox(UASSystemClass);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        comboBox_portName = new QLabel(groupBox);
        comboBox_portName->setObjectName(QStringLiteral("comboBox_portName"));
        comboBox_portName->setMinimumSize(QSize(100, 0));
        comboBox_portName->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_3->addWidget(comboBox_portName);

        cb_portName = new QComboBox(groupBox);
        cb_portName->setObjectName(QStringLiteral("cb_portName"));

        horizontalLayout_3->addWidget(cb_portName);


        gridLayout->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        comboBox_baudRate = new QLabel(groupBox);
        comboBox_baudRate->setObjectName(QStringLiteral("comboBox_baudRate"));
        comboBox_baudRate->setMinimumSize(QSize(100, 0));
        comboBox_baudRate->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_14->addWidget(comboBox_baudRate);

        cb_baudRate = new QComboBox(groupBox);
        cb_baudRate->setObjectName(QStringLiteral("cb_baudRate"));

        horizontalLayout_14->addWidget(cb_baudRate);


        gridLayout->addLayout(horizontalLayout_14, 1, 0, 1, 1);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(100, 0));
        label_3->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_17->addWidget(label_3);

        cb_stop_byte = new QComboBox(groupBox);
        cb_stop_byte->setObjectName(QStringLiteral("cb_stop_byte"));

        horizontalLayout_17->addWidget(cb_stop_byte);


        gridLayout->addLayout(horizontalLayout_17, 2, 0, 1, 1);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(100, 0));
        label_2->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_15->addWidget(label_2);

        cb_databyte = new QComboBox(groupBox);
        cb_databyte->setObjectName(QStringLiteral("cb_databyte"));

        horizontalLayout_15->addWidget(cb_databyte);


        gridLayout->addLayout(horizontalLayout_15, 3, 0, 1, 1);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(100, 0));
        label->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_16->addWidget(label);

        cb_check = new QComboBox(groupBox);
        cb_check->setObjectName(QStringLiteral("cb_check"));

        horizontalLayout_16->addWidget(cb_check);


        gridLayout->addLayout(horizontalLayout_16, 4, 0, 1, 1);

        pb_open = new QPushButton(groupBox);
        pb_open->setObjectName(QStringLiteral("pb_open"));

        gridLayout->addWidget(pb_open, 5, 0, 1, 1);


        gridLayout_30->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(UASSystemClass);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_24 = new QGridLayout(groupBox_2);
        gridLayout_24->setSpacing(6);
        gridLayout_24->setContentsMargins(11, 11, 11, 11);
        gridLayout_24->setObjectName(QStringLiteral("gridLayout_24"));
        tableWidget = new QTableWidget(groupBox_2);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
        tableWidget->setSizePolicy(sizePolicy);

        gridLayout_24->addWidget(tableWidget, 0, 0, 1, 1);


        gridLayout_30->addWidget(groupBox_2, 0, 1, 1, 1);

        widget = new QWidget(UASSystemClass);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setMaximumSize(QSize(540, 16777215));
        gridLayout_29 = new QGridLayout(widget);
        gridLayout_29->setSpacing(6);
        gridLayout_29->setContentsMargins(11, 11, 11, 11);
        gridLayout_29->setObjectName(QStringLiteral("gridLayout_29"));
        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setMaximumSize(QSize(500, 16777215));
        gridLayout_25 = new QGridLayout(widget_2);
        gridLayout_25->setSpacing(6);
        gridLayout_25->setContentsMargins(11, 11, 11, 11);
        gridLayout_25->setObjectName(QStringLiteral("gridLayout_25"));
        groupBox_3 = new QGroupBox(widget_2);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_2 = new QGridLayout(groupBox_3);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_27 = new QGridLayout();
        gridLayout_27->setSpacing(6);
        gridLayout_27->setObjectName(QStringLiteral("gridLayout_27"));
        label_25 = new QLabel(groupBox_3);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setMinimumSize(QSize(40, 0));
        label_25->setMaximumSize(QSize(40, 16777215));

        gridLayout_27->addWidget(label_25, 0, 0, 1, 1);

        basicLat = new QLineEdit(groupBox_3);
        basicLat->setObjectName(QStringLiteral("basicLat"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(basicLat->sizePolicy().hasHeightForWidth());
        basicLat->setSizePolicy(sizePolicy1);
        basicLat->setMaximumSize(QSize(10000, 16777215));

        gridLayout_27->addWidget(basicLat, 0, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout_27, 1, 0, 1, 1);

        gridLayout_28 = new QGridLayout();
        gridLayout_28->setSpacing(6);
        gridLayout_28->setObjectName(QStringLiteral("gridLayout_28"));
        label_26 = new QLabel(groupBox_3);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setMinimumSize(QSize(40, 0));
        label_26->setMaximumSize(QSize(40, 16777215));

        gridLayout_28->addWidget(label_26, 0, 0, 1, 1);

        basicHeight = new QLineEdit(groupBox_3);
        basicHeight->setObjectName(QStringLiteral("basicHeight"));
        sizePolicy1.setHeightForWidth(basicHeight->sizePolicy().hasHeightForWidth());
        basicHeight->setSizePolicy(sizePolicy1);
        basicHeight->setMaximumSize(QSize(10000, 16777215));

        gridLayout_28->addWidget(basicHeight, 0, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout_28, 2, 0, 1, 1);

        gridLayout_26 = new QGridLayout();
        gridLayout_26->setSpacing(6);
        gridLayout_26->setObjectName(QStringLiteral("gridLayout_26"));
        label_24 = new QLabel(groupBox_3);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setMinimumSize(QSize(40, 0));
        label_24->setMaximumSize(QSize(40, 16777215));

        gridLayout_26->addWidget(label_24, 0, 0, 1, 1);

        basicLong = new QLineEdit(groupBox_3);
        basicLong->setObjectName(QStringLiteral("basicLong"));
        sizePolicy1.setHeightForWidth(basicLong->sizePolicy().hasHeightForWidth());
        basicLong->setSizePolicy(sizePolicy1);
        basicLong->setMaximumSize(QSize(10000, 16777215));

        gridLayout_26->addWidget(basicLong, 0, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout_26, 0, 0, 1, 1);


        gridLayout_25->addWidget(groupBox_3, 0, 0, 1, 1);

        pb_Person = new QPushButton(widget_2);
        pb_Person->setObjectName(QStringLiteral("pb_Person"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pb_Person->sizePolicy().hasHeightForWidth());
        pb_Person->setSizePolicy(sizePolicy2);
        pb_Person->setMaximumSize(QSize(200, 16777215));

        gridLayout_25->addWidget(pb_Person, 0, 1, 1, 1);


        gridLayout_29->addWidget(widget_2, 0, 0, 1, 1);

        groupBox1 = new QGroupBox(widget);
        groupBox1->setObjectName(QStringLiteral("groupBox1"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(groupBox1->sizePolicy().hasHeightForWidth());
        groupBox1->setSizePolicy(sizePolicy3);
        groupBox1->setMinimumSize(QSize(500, 0));
        groupBox1->setMaximumSize(QSize(500, 16777215));
        gridLayout_23 = new QGridLayout(groupBox1);
        gridLayout_23->setSpacing(6);
        gridLayout_23->setContentsMargins(11, 11, 11, 11);
        gridLayout_23->setObjectName(QStringLiteral("gridLayout_23"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_4 = new QLabel(groupBox1);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(150, 0));
        label_4->setMaximumSize(QSize(150, 16777215));

        gridLayout_3->addWidget(label_4, 0, 0, 1, 1);

        dcgdTime = new QLineEdit(groupBox1);
        dcgdTime->setObjectName(QStringLiteral("dcgdTime"));

        gridLayout_3->addWidget(dcgdTime, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_3, 0, 0, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_5 = new QLabel(groupBox1);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(150, 0));
        label_5->setMaximumSize(QSize(150, 16777215));

        gridLayout_4->addWidget(label_5, 0, 0, 1, 1);

        dcgdStat = new QLineEdit(groupBox1);
        dcgdStat->setObjectName(QStringLiteral("dcgdStat"));

        gridLayout_4->addWidget(dcgdStat, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_4, 1, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        label_6 = new QLabel(groupBox1);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(150, 0));
        label_6->setMaximumSize(QSize(150, 16777215));

        gridLayout_5->addWidget(label_6, 0, 0, 1, 1);

        dcGPSTime = new QLineEdit(groupBox1);
        dcGPSTime->setObjectName(QStringLiteral("dcGPSTime"));

        gridLayout_5->addWidget(dcGPSTime, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_5, 2, 0, 1, 1);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        label_7 = new QLabel(groupBox1);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(150, 0));
        label_7->setMaximumSize(QSize(150, 16777215));

        gridLayout_6->addWidget(label_7, 0, 0, 1, 1);

        dcGPSLong = new QLineEdit(groupBox1);
        dcGPSLong->setObjectName(QStringLiteral("dcGPSLong"));

        gridLayout_6->addWidget(dcGPSLong, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_6, 3, 0, 1, 1);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        label_8 = new QLabel(groupBox1);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setMinimumSize(QSize(150, 0));
        label_8->setMaximumSize(QSize(150, 16777215));

        gridLayout_7->addWidget(label_8, 0, 0, 1, 1);

        dcGPSLat = new QLineEdit(groupBox1);
        dcGPSLat->setObjectName(QStringLiteral("dcGPSLat"));

        gridLayout_7->addWidget(dcGPSLat, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_7, 4, 0, 1, 1);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        label_9 = new QLabel(groupBox1);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setMinimumSize(QSize(150, 0));
        label_9->setMaximumSize(QSize(150, 16777215));

        gridLayout_8->addWidget(label_9, 0, 0, 1, 1);

        dcGPSHeight = new QLineEdit(groupBox1);
        dcGPSHeight->setObjectName(QStringLiteral("dcGPSHeight"));

        gridLayout_8->addWidget(dcGPSHeight, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_8, 5, 0, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        label_10 = new QLabel(groupBox1);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setMinimumSize(QSize(150, 0));
        label_10->setMaximumSize(QSize(150, 16777215));

        gridLayout_9->addWidget(label_10, 0, 0, 1, 1);

        bbRTKTime = new QLineEdit(groupBox1);
        bbRTKTime->setObjectName(QStringLiteral("bbRTKTime"));

        gridLayout_9->addWidget(bbRTKTime, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_9, 6, 0, 1, 1);

        gridLayout_10 = new QGridLayout();
        gridLayout_10->setSpacing(6);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        label_11 = new QLabel(groupBox1);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setMinimumSize(QSize(150, 0));
        label_11->setMaximumSize(QSize(150, 16777215));

        gridLayout_10->addWidget(label_11, 0, 0, 1, 1);

        bbRTKLong = new QLineEdit(groupBox1);
        bbRTKLong->setObjectName(QStringLiteral("bbRTKLong"));

        gridLayout_10->addWidget(bbRTKLong, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_10, 7, 0, 1, 1);

        gridLayout_11 = new QGridLayout();
        gridLayout_11->setSpacing(6);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        label_12 = new QLabel(groupBox1);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setMinimumSize(QSize(150, 0));
        label_12->setMaximumSize(QSize(150, 16777215));

        gridLayout_11->addWidget(label_12, 0, 0, 1, 1);

        bbRTKLat = new QLineEdit(groupBox1);
        bbRTKLat->setObjectName(QStringLiteral("bbRTKLat"));

        gridLayout_11->addWidget(bbRTKLat, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_11, 8, 0, 1, 1);

        gridLayout_12 = new QGridLayout();
        gridLayout_12->setSpacing(6);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        label_13 = new QLabel(groupBox1);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setMinimumSize(QSize(150, 0));
        label_13->setMaximumSize(QSize(150, 16777215));

        gridLayout_12->addWidget(label_13, 0, 0, 1, 1);

        bbRTKHeight = new QLineEdit(groupBox1);
        bbRTKHeight->setObjectName(QStringLiteral("bbRTKHeight"));

        gridLayout_12->addWidget(bbRTKHeight, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_12, 9, 0, 1, 1);

        gridLayout_13 = new QGridLayout();
        gridLayout_13->setSpacing(6);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        label_14 = new QLabel(groupBox1);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setMinimumSize(QSize(150, 0));
        label_14->setMaximumSize(QSize(150, 16777215));

        gridLayout_13->addWidget(label_14, 0, 0, 1, 1);

        destTime = new QLineEdit(groupBox1);
        destTime->setObjectName(QStringLiteral("destTime"));

        gridLayout_13->addWidget(destTime, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_13, 10, 0, 1, 1);

        gridLayout_14 = new QGridLayout();
        gridLayout_14->setSpacing(6);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        label_15 = new QLabel(groupBox1);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setMinimumSize(QSize(150, 0));
        label_15->setMaximumSize(QSize(150, 16777215));

        gridLayout_14->addWidget(label_15, 0, 0, 1, 1);

        destStat = new QLineEdit(groupBox1);
        destStat->setObjectName(QStringLiteral("destStat"));

        gridLayout_14->addWidget(destStat, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_14, 11, 0, 1, 1);

        gridLayout_15 = new QGridLayout();
        gridLayout_15->setSpacing(6);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        label_16 = new QLabel(groupBox1);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setMinimumSize(QSize(150, 0));
        label_16->setMaximumSize(QSize(150, 16777215));

        gridLayout_15->addWidget(label_16, 0, 0, 1, 1);

        destDistance = new QLineEdit(groupBox1);
        destDistance->setObjectName(QStringLiteral("destDistance"));

        gridLayout_15->addWidget(destDistance, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_15, 12, 0, 1, 1);

        gridLayout_16 = new QGridLayout();
        gridLayout_16->setSpacing(6);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        label_17 = new QLabel(groupBox1);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setMinimumSize(QSize(150, 0));
        label_17->setMaximumSize(QSize(150, 16777215));

        gridLayout_16->addWidget(label_17, 0, 0, 1, 1);

        destMeauTime = new QLineEdit(groupBox1);
        destMeauTime->setObjectName(QStringLiteral("destMeauTime"));

        gridLayout_16->addWidget(destMeauTime, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_16, 13, 0, 1, 1);

        gridLayout_17 = new QGridLayout();
        gridLayout_17->setSpacing(6);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        label_18 = new QLabel(groupBox1);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setMinimumSize(QSize(150, 0));
        label_18->setMaximumSize(QSize(150, 16777215));

        gridLayout_17->addWidget(label_18, 0, 0, 1, 1);

        dcTime = new QLineEdit(groupBox1);
        dcTime->setObjectName(QStringLiteral("dcTime"));

        gridLayout_17->addWidget(dcTime, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_17, 14, 0, 1, 1);

        gridLayout_18 = new QGridLayout();
        gridLayout_18->setSpacing(6);
        gridLayout_18->setObjectName(QStringLiteral("gridLayout_18"));
        label_19 = new QLabel(groupBox1);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setMinimumSize(QSize(150, 0));
        label_19->setMaximumSize(QSize(150, 16777215));

        gridLayout_18->addWidget(label_19, 0, 0, 1, 1);

        jzdcStat = new QLineEdit(groupBox1);
        jzdcStat->setObjectName(QStringLiteral("jzdcStat"));

        gridLayout_18->addWidget(jzdcStat, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_18, 15, 0, 1, 1);

        gridLayout_19 = new QGridLayout();
        gridLayout_19->setSpacing(6);
        gridLayout_19->setObjectName(QStringLiteral("gridLayout_19"));
        label_20 = new QLabel(groupBox1);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setMinimumSize(QSize(150, 0));
        label_20->setMaximumSize(QSize(150, 16777215));

        gridLayout_19->addWidget(label_20, 0, 0, 1, 1);

        wrjRTKTime = new QLineEdit(groupBox1);
        wrjRTKTime->setObjectName(QStringLiteral("wrjRTKTime"));

        gridLayout_19->addWidget(wrjRTKTime, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_19, 16, 0, 1, 1);

        gridLayout_20 = new QGridLayout();
        gridLayout_20->setSpacing(6);
        gridLayout_20->setObjectName(QStringLiteral("gridLayout_20"));
        label_21 = new QLabel(groupBox1);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setMinimumSize(QSize(150, 0));
        label_21->setMaximumSize(QSize(150, 16777215));

        gridLayout_20->addWidget(label_21, 0, 0, 1, 1);

        wrjLong = new QLineEdit(groupBox1);
        wrjLong->setObjectName(QStringLiteral("wrjLong"));

        gridLayout_20->addWidget(wrjLong, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_20, 17, 0, 1, 1);

        gridLayout_21 = new QGridLayout();
        gridLayout_21->setSpacing(6);
        gridLayout_21->setObjectName(QStringLiteral("gridLayout_21"));
        label_22 = new QLabel(groupBox1);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setMinimumSize(QSize(150, 0));
        label_22->setMaximumSize(QSize(150, 16777215));

        gridLayout_21->addWidget(label_22, 0, 0, 1, 1);

        wrjLat = new QLineEdit(groupBox1);
        wrjLat->setObjectName(QStringLiteral("wrjLat"));

        gridLayout_21->addWidget(wrjLat, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_21, 18, 0, 1, 1);

        gridLayout_22 = new QGridLayout();
        gridLayout_22->setSpacing(6);
        gridLayout_22->setObjectName(QStringLiteral("gridLayout_22"));
        label_23 = new QLabel(groupBox1);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setMinimumSize(QSize(150, 0));
        label_23->setMaximumSize(QSize(150, 16777215));

        gridLayout_22->addWidget(label_23, 0, 0, 1, 1);

        wrjHeight = new QLineEdit(groupBox1);
        wrjHeight->setObjectName(QStringLiteral("wrjHeight"));

        gridLayout_22->addWidget(wrjHeight, 0, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_22, 19, 0, 1, 1);


        gridLayout_29->addWidget(groupBox1, 1, 0, 1, 1);


        gridLayout_30->addWidget(widget, 0, 2, 2, 1);

        w_Plot = new QWidget(UASSystemClass);
        w_Plot->setObjectName(QStringLiteral("w_Plot"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(w_Plot->sizePolicy().hasHeightForWidth());
        w_Plot->setSizePolicy(sizePolicy4);

        gridLayout_30->addWidget(w_Plot, 1, 0, 1, 2);


        retranslateUi(UASSystemClass);

        QMetaObject::connectSlotsByName(UASSystemClass);
    } // setupUi

    void retranslateUi(QWidget *UASSystemClass)
    {
        UASSystemClass->setWindowTitle(QApplication::translate("UASSystemClass", "UASSystem", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("UASSystemClass", "\344\270\262\345\217\243\350\256\276\347\275\256", Q_NULLPTR));
        comboBox_portName->setText(QApplication::translate("UASSystemClass", "\344\270\262\345\217\243\351\200\211\346\213\251:", Q_NULLPTR));
        comboBox_baudRate->setText(QApplication::translate("UASSystemClass", "\346\263\242\347\211\271\347\216\207:", Q_NULLPTR));
        cb_baudRate->clear();
        cb_baudRate->insertItems(0, QStringList()
         << QApplication::translate("UASSystemClass", "2400", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "4800", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "9600", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "19200", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "57600", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "115200", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "128000", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "2304000", Q_NULLPTR)
        );
        label_3->setText(QApplication::translate("UASSystemClass", "\345\201\234\346\255\242\344\275\215:", Q_NULLPTR));
        cb_stop_byte->clear();
        cb_stop_byte->insertItems(0, QStringList()
         << QApplication::translate("UASSystemClass", "1", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "2", Q_NULLPTR)
        );
        label_2->setText(QApplication::translate("UASSystemClass", "\346\225\260\346\215\256\344\275\215:", Q_NULLPTR));
        cb_databyte->clear();
        cb_databyte->insertItems(0, QStringList()
         << QApplication::translate("UASSystemClass", "8", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "7", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "6", Q_NULLPTR)
        );
        label->setText(QApplication::translate("UASSystemClass", "\345\245\207\345\201\266\346\240\241\351\252\214:", Q_NULLPTR));
        cb_check->clear();
        cb_check->insertItems(0, QStringList()
         << QApplication::translate("UASSystemClass", "NONE", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "ODD", Q_NULLPTR)
         << QApplication::translate("UASSystemClass", "EVE", Q_NULLPTR)
        );
        pb_open->setText(QApplication::translate("UASSystemClass", "\346\211\223\345\274\200\344\270\262\345\217\243", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("UASSystemClass", "\350\256\241\347\256\227\347\273\223\346\236\234", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("UASSystemClass", "\345\237\272\345\207\206\344\275\215\347\275\256", Q_NULLPTR));
        label_25->setText(QApplication::translate("UASSystemClass", "\347\272\254\345\272\246:", Q_NULLPTR));
        label_26->setText(QApplication::translate("UASSystemClass", "\351\253\230\345\272\246:", Q_NULLPTR));
        label_24->setText(QApplication::translate("UASSystemClass", "\347\273\217\345\272\246:", Q_NULLPTR));
        pb_Person->setText(QApplication::translate("UASSystemClass", "\344\270\252\344\272\272\344\277\241\346\201\257", Q_NULLPTR));
        groupBox1->setTitle(QApplication::translate("UASSystemClass", "\346\227\240\344\272\272\346\234\272\345\217\212\351\235\266\346\240\207\346\225\260\346\215\256", Q_NULLPTR));
        label_4->setText(QApplication::translate("UASSystemClass", "\345\220\212\350\210\261\346\203\257\345\257\274\346\227\266\351\227\264:", Q_NULLPTR));
        label_5->setText(QApplication::translate("UASSystemClass", "\345\220\212\350\210\261\346\203\257\345\257\274\345\247\277\346\200\201:", Q_NULLPTR));
        label_6->setText(QApplication::translate("UASSystemClass", "\345\220\212\350\210\261GPS\346\227\266\351\227\264:", Q_NULLPTR));
        label_7->setText(QApplication::translate("UASSystemClass", "\345\220\212\350\210\261GPS\347\273\217\345\272\246:", Q_NULLPTR));
        label_8->setText(QApplication::translate("UASSystemClass", "\345\220\212\350\210\261GPS\347\272\254\345\272\246:", Q_NULLPTR));
        label_9->setText(QApplication::translate("UASSystemClass", "\345\220\212\350\210\261GPS\351\253\230\345\272\246:", Q_NULLPTR));
        label_10->setText(QApplication::translate("UASSystemClass", "\346\240\207\351\235\266RTK\346\227\266\351\227\264:", Q_NULLPTR));
        label_11->setText(QApplication::translate("UASSystemClass", "\346\240\207\351\235\266RTK\347\273\217\345\272\246:", Q_NULLPTR));
        label_12->setText(QApplication::translate("UASSystemClass", "\346\240\207\351\235\266RTK\347\272\254\345\272\246:", Q_NULLPTR));
        label_13->setText(QApplication::translate("UASSystemClass", "\346\240\207\351\235\266RTK\351\253\230\345\272\246:", Q_NULLPTR));
        label_14->setText(QApplication::translate("UASSystemClass", "\347\233\256\346\240\207\346\227\266\351\227\264:", Q_NULLPTR));
        label_15->setText(QApplication::translate("UASSystemClass", "\347\233\256\346\240\207\345\247\277\346\200\201:", Q_NULLPTR));
        label_16->setText(QApplication::translate("UASSystemClass", "\347\233\256\346\240\207\350\267\235\347\246\273:", Q_NULLPTR));
        label_17->setText(QApplication::translate("UASSystemClass", "\347\233\256\346\240\207\350\267\235\347\246\273\346\265\213\351\207\217\346\227\266\351\227\264:", Q_NULLPTR));
        label_18->setText(QApplication::translate("UASSystemClass", "\345\220\212\350\210\261\346\227\266\351\227\264:", Q_NULLPTR));
        label_19->setText(QApplication::translate("UASSystemClass", "\346\234\272\350\275\275\345\220\212\350\210\261\345\247\277\346\200\201:", Q_NULLPTR));
        label_20->setText(QApplication::translate("UASSystemClass", "\346\227\240\344\272\272\346\234\272RTK\346\227\266\351\227\264:", Q_NULLPTR));
        label_21->setText(QApplication::translate("UASSystemClass", "\346\227\240\344\272\272\346\234\272\347\273\217\345\272\246:", Q_NULLPTR));
        label_22->setText(QApplication::translate("UASSystemClass", "\346\227\240\344\272\272\346\234\272\347\272\254\345\272\246:", Q_NULLPTR));
        label_23->setText(QApplication::translate("UASSystemClass", "\346\227\240\344\272\272\346\234\272\351\253\230\345\272\246:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UASSystemClass: public Ui_UASSystemClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UASSYSTEM_H
