/********************************************************************************
** Form generated from reading UI file 'UserPlot.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERPLOT_H
#define UI_USERPLOT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserPlot
{
public:
    QGridLayout *gridLayout;
    QWidget *w_Plot;

    void setupUi(QWidget *UserPlot)
    {
        if (UserPlot->objectName().isEmpty())
            UserPlot->setObjectName(QStringLiteral("UserPlot"));
        UserPlot->resize(970, 544);
        gridLayout = new QGridLayout(UserPlot);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        w_Plot = new QWidget(UserPlot);
        w_Plot->setObjectName(QStringLiteral("w_Plot"));

        gridLayout->addWidget(w_Plot, 0, 0, 1, 1);


        retranslateUi(UserPlot);

        QMetaObject::connectSlotsByName(UserPlot);
    } // setupUi

    void retranslateUi(QWidget *UserPlot)
    {
        UserPlot->setWindowTitle(QApplication::translate("UserPlot", "UserPlot", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UserPlot: public Ui_UserPlot {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERPLOT_H
