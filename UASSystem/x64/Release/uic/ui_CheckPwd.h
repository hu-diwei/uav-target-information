/********************************************************************************
** Form generated from reading UI file 'CheckPwd.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHECKPWD_H
#define UI_CHECKPWD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CheckPwd
{
public:
    QGridLayout *gridLayout_4;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QWidget *widget;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *le_Pwd;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pb_Check;
    QPushButton *pb_Back;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *CheckPwd)
    {
        if (CheckPwd->objectName().isEmpty())
            CheckPwd->setObjectName(QStringLiteral("CheckPwd"));
        CheckPwd->resize(465, 240);
        gridLayout_4 = new QGridLayout(CheckPwd);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        verticalSpacer = new QSpacerItem(20, 57, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(69, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_2, 1, 0, 1, 1);

        widget = new QWidget(CheckPwd);
        widget->setObjectName(QStringLiteral("widget"));
        gridLayout_3 = new QGridLayout(widget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        le_Pwd = new QLineEdit(widget);
        le_Pwd->setObjectName(QStringLiteral("le_Pwd"));

        gridLayout->addWidget(le_Pwd, 0, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout, 0, 0, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 0, 1, 1);

        pb_Check = new QPushButton(widget);
        pb_Check->setObjectName(QStringLiteral("pb_Check"));

        gridLayout_2->addWidget(pb_Check, 0, 1, 1, 1);

        pb_Back = new QPushButton(widget);
        pb_Back->setObjectName(QStringLiteral("pb_Back"));

        gridLayout_2->addWidget(pb_Back, 0, 2, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 1, 0, 1, 1);


        gridLayout_4->addWidget(widget, 1, 1, 1, 2);

        horizontalSpacer_3 = new QSpacerItem(69, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_3, 1, 3, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 56, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_2, 2, 2, 1, 1);


        retranslateUi(CheckPwd);

        QMetaObject::connectSlotsByName(CheckPwd);
    } // setupUi

    void retranslateUi(QWidget *CheckPwd)
    {
        CheckPwd->setWindowTitle(QApplication::translate("CheckPwd", "CheckPwd", Q_NULLPTR));
        label->setText(QApplication::translate("CheckPwd", "\350\257\267\350\276\223\345\205\245\345\257\206\347\240\201:", Q_NULLPTR));
        pb_Check->setText(QApplication::translate("CheckPwd", "\351\252\214\350\257\201", Q_NULLPTR));
        pb_Back->setText(QApplication::translate("CheckPwd", "\350\277\224\345\233\236", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CheckPwd: public Ui_CheckPwd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHECKPWD_H
