#pragma once

#include <QWidget>
#include "ui_UserPlot.h"
#include "Commdef.h"
#include "qcustomplot.h"

class UserPlot : public QWidget
{
	Q_OBJECT

public:
	UserPlot(QWidget *parent = Q_NULLPTR);
	~UserPlot();

	void init();	//初始化
	void connectslots();	//连接信号与槽
	void setInitWidget();	//设置初始化显示界面
	void addPlotToWidget();		//添加折线图
	void initPlot();	//初始化折线图

	void startTimer();	//启动定时器
	void stopTimer();	//停止定时器
	void flushPlot();	//刷新折线图	
	double getMaxXY(QVector<double> datas);	//获取Y最大值


	public slots:
	void slt_flushData();

public:
	QCustomPlot *m_Plot;
	QTimer *timer;	//定时器



private:
	Ui::UserPlot ui;
};
