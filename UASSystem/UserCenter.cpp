#include "UserCenter.h"

UserCenter::UserCenter(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	init();
	connectslots();
}

UserCenter::~UserCenter()
{
}

void UserCenter::init()	//初始化
{
	ui.rb_Man->setChecked(true);
	ui.le_Pwd->setEchoMode(QLineEdit::Password);
	ui.le_ID->setEnabled(false);
}
void UserCenter::connectslots()	//连接信号与槽
{
	connect(ui.pb_Back, SIGNAL(clicked()), this, SLOT(slt_Back()));
	connect(ui.pb_Regis, SIGNAL(clicked()), this, SLOT(slt_Reset()));
}
void UserCenter::setInitWidget()
{
	showUserInfo();
}

void UserCenter::slt_Reset()	//修改
{
	User user = getUser();
	user.id = gCurrLoginUser.id;
	if (user.userID == "" || user.userPwd == "")
	{
		gWarnning("存在未填写项,注册失败!");
		return;
	}
	gMysqlFunc.updateUserTable(gError, user);
	gCurrLoginUser = user;
	gWarnning("修改成功!");
	return;

}
void UserCenter::slt_Back()	//返回
{
	this->close();
}

User UserCenter::getUser()	//获取用户
{
	User user;
	user.userID = qstr2str(ui.le_ID->text());
	user.userName = qstr2str(ui.le_Name->text());
	user.userPwd = qstr2str(ui.le_Pwd->text());
	user.gender = qstr2str("男");
	if (ui.rb_Women->isChecked())
	{
		user.gender = qstr2str("女");
	}
	user.age = ui.le_Age->text().toInt();
	return user;
}
void UserCenter::clearUserInfo()	//清空用户信息
{
	ui.le_ID->setText("");
	ui.le_Pwd->setText("");
	ui.le_Name->setText("");
	ui.le_Age->setText("");
}


void UserCenter::showUserInfo()
{
	QString gender = str2qstr(gCurrLoginUser.gender);
	ui.rb_Women->setChecked(true);
	ui.le_ID->setText(str2qstr(gCurrLoginUser.userID));
	ui.le_Pwd->setText(str2qstr(gCurrLoginUser.userPwd));
	ui.le_Name->setText(str2qstr(gCurrLoginUser.userName));
	ui.le_Age->setText(QString::number(gCurrLoginUser.age));
	if (gender == "男")
	{
		ui.rb_Man->setChecked(true);
	}
}