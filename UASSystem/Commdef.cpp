#include "Commdef.h"

string gError;
MysqlFunc gMysqlFunc;
User gCurrLoginUser;
QMap<string, User> gAllUsers;
QQueue<QString> gAllSerialData;	//接收串口数据
DataInfo gCurrData;	//当前数据信息
QMutex gSerialDataMutex;	//串口数据队列锁
QMutex gDataInfoMutex;	//当前数据锁
QVector<double> gXData, gYData;	//曲线数据
QMutex plotMutex;	//曲线锁

void gSetTableWidgetMode(QTableWidget *tw)
{
	tw->setSelectionMode(QAbstractItemView::SingleSelection);
	tw->setSelectionBehavior(QAbstractItemView::SelectRows);	//整行选中
	tw->setEditTriggers(QAbstractItemView::NoEditTriggers);	//设置不可编辑
}

void gWarnning(string info)
{
	QString error = QString::fromStdString(info);
	QMessageBox::warning(NULL, "Warning", error, QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
	return;
}

QString str2qstr(const string str)
{
	return QString::fromLocal8Bit(str.data());
}

string qstr2str(const QString qstr)
{
	QByteArray cdata = qstr.toLocal8Bit();
	return string(cdata);
}
