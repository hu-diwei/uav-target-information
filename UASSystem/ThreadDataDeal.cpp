#include "ThreadDataDeal.h"

ThreadDataDeal::ThreadDataDeal(QObject *parent)
	: QThread(parent)
{
	m_isRunning = true;
}

ThreadDataDeal::~ThreadDataDeal()
{
}

void ThreadDataDeal::run()
{
	QString currData;
	while (1)
	{
		if (m_isRunning == false)
		{
			break;
		}
		if (gAllSerialData.size() > 0)
		{
			gSerialDataMutex.lock();
			currData = gAllSerialData.dequeue();	  //删除当前队列第一个元素,并返回这个元素
			gSerialDataMutex.unlock();
			dealData(currData);
		}
		else
		{
			Sleep(30);
		}
	}
}

void ThreadDataDeal::dealData(QString data)
{
	DataInfo info;
	QStringList list = data.split(",");
	if (list.size() == 23)
	{
		info.basicLong = list[0];
		info.basicLat = list[1];
		info.basicHeight = list[2];

		info.dcgdTime = list[3];
		info.dcgdStat = list[4];

		info.dcGPSTime = list[5];
		info.dcGPSLong = list[6];
		info.dcGPSLat = list[7];
		info.dcGPSHeight = list[8];

		info.bbRTKTime = list[9];
		info.bbRTKLong = list[10];
		info.bbRTKLat = list[11];
		info.bbRTKHeight = list[12];

		info.destTime = list[13];
		info.destStat = list[14];
		info.destDistance = list[15];
		info.destMeauTime = list[16];

		info.dcTime = list[17];
		info.jzdcStat = list[18];

		info.wrjRTKTime = list[19];
		info.wrjLong = list[20];
		info.wrjLat = list[21];
		info.wrjHeight = list[22];

		gDataInfoMutex.lock();
		gCurrData = info;
		gDataInfoMutex.unlock();
		plotMutex.lock();
		gXData.push_back(calcTime(info.destTime));
		gYData.push_back(calcDestLong(info.bbRTKLong, info.wrjLong));
		plotMutex.unlock();
	}
}


double ThreadDataDeal::calcTime(QString time)
{
	int year, month, day, hour, min, sec, msec;	//年月日时分秒毫秒
	string temp = qstr2str(time);
	sscanf(temp.c_str(), "%d#%d#%d#%d#%d#%d#%d", &year, &month, &day, &hour, &min, &sec, &msec);
	double result = (double)sec + (double)msec / 1000;
	return result;
}

double ThreadDataDeal::calcDestLong(QString long1, QString long2)
{
	double l1 = long1.toDouble();
	double l2 = long2.toDouble();
	return (l1 + l2) / 2;
}